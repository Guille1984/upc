package pe.edu.cibertec.examen.service;

import java.util.List;
import pe.edu.cibertec.examen.model.Factura;

public interface IFacturaService {
	
	Factura registrar(Factura factura);
	Factura actualizar(Factura factura);
	void eliminar (String numero);
	List<Factura> listarFacturas();
}