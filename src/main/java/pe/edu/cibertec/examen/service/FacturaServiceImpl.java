package pe.edu.cibertec.examen.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.edu.cibertec.examen.model.Factura;
import pe.edu.cibertec.examen.repository.FacturaRepo;

@Service
public class FacturaServiceImpl implements IFacturaService {
	
	@Autowired
	FacturaRepo facturaRepo;

	@Override
	public Factura registrar(Factura factura) {
		// TODO Auto-generated method stub
		return facturaRepo.save(factura);
	}

	@Override
	public void eliminar(String numero) {
		// TODO Auto-generated method stub
		facturaRepo.deleteById(numero);
	}

	@Override
	public List<Factura> listarFacturas() {
		// TODO Auto-generated method stub
		return facturaRepo.findAll();
	}

	@Override
	public Factura actualizar(Factura factura) {
		// TODO Auto-generated method stub
		return facturaRepo.save(factura);
	}
	
}