package pe.edu.cibertec.examen.model;

import java.util.Date;
import javax.persistence.*;
import lombok.Data;

@Data
@Entity
public class Factura {
	
	@Id
	private String numero;
	private Date fecha;
	private String cliente;
	private double monto;

}