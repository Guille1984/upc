package pe.edu.cibertec.examen.model;

public class Cliente extends Persona {
	
	private String tipoCliente;

	public String getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

}
