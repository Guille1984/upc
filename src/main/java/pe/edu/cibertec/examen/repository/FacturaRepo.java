package pe.edu.cibertec.examen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.edu.cibertec.examen.model.Factura;

public interface FacturaRepo extends JpaRepository<Factura, String> {

}