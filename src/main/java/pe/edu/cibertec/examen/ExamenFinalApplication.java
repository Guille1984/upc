package pe.edu.cibertec.examen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pe.edu.cibertec.examen.utils.Excepciones;

@SpringBootApplication
public class ExamenFinalApplication {
	
	public static void main(String[] args) throws Excepciones {
		SpringApplication.run(ExamenFinalApplication.class, args);
	}

}