package pe.edu.cibertec.examen.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pe.edu.cibertec.examen.model.Factura;
import pe.edu.cibertec.examen.service.*;

@Component
public class FacturaController {
	
	@Autowired
	public IFacturaService service;
	
	List<Factura> lista = new ArrayList<>();
	
	public void guardar(Factura factura) {
		service.registrar(factura);
	}
	
	public void actualizar(Factura factura) {
		service.actualizar(factura);
	}
	
	public void listar() {
		
		lista = service.listarFacturas();
		System.out.println("Listado: " + lista);
	}
	
	public void insertarFactura() {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		String numero = sc.nextLine();
		String fecha = sc.nextLine();
		String cliente = sc.nextLine();
		String monto = sc.nextLine();
		System.out.println("codigo es: " + numero);
		System.out.println("fecha es: " + fecha);
		System.out.println("cliente es: " + cliente);
		System.out.println("monto es: " + monto);
		Factura factura = new Factura();
		factura.setNumero(numero);
		factura.setFecha(Date.valueOf(fecha));
		factura.setCliente(cliente);
		factura.setMonto(Double.parseDouble(monto));
		guardar(factura);
	}

}