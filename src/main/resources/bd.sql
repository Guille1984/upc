CREATE TABLE `cibertec`.`tb_factura` (
  `numero` VARCHAR(10) NOT NULL,
  `fecha` DATETIME NULL,
  `cliente` VARCHAR(11) NULL,
  `monto` DECIMAL(12,2) NULL,
  PRIMARY KEY (`numero`));
CREATE TABLE `cibertec`.`tb_cliente` (
  `ruc` VARCHAR(11) NOT NULL,
  `razon_social` VARCHAR(50) NULL,
  `direccion` VARCHAR(100) NULL,
  PRIMARY KEY (`ruc`));
  ALTER TABLE `cibertec`.`tb_factura` 
ADD INDEX `cliente_idx` (`cliente` ASC) VISIBLE;
;
ALTER TABLE `cibertec`.`tb_factura` 
ADD CONSTRAINT `cliente`
  FOREIGN KEY (`cliente`)
  REFERENCES `cibertec`.`tb_cliente` (`ruc`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  INSERT INTO `cibertec`.`tb_cliente` (`ruc`, `razon_social`, `direccion`) VALUES ('12345678911', 'Los Jotitas', 'Calle peloteros 123');
  INSERT INTO `cibertec`.`tb_cliente` (`ruc`, `razon_social`, `direccion`) VALUES ('22334455667', 'Polos Peru', 'Av. Gamarra 123');
  INSERT INTO `cibertec`.`tb_factura` (`numero`, `fecha`, `cliente`, `monto`) VALUES ('10001', '2021-06-04', '12345678911', '987.34');
  INSERT INTO `cibertec`.`tb_factura` (`numero`, `fecha`, `cliente`, `monto`) VALUES ('10002', '2021-06-04 22:01:30', '22334455667', '534.19');