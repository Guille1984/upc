package pe.edu.cibertec.examen.repository;

import java.sql.Date;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import pe.edu.cibertec.examen.model.Factura;
import pe.edu.cibertec.examen.service.IFacturaService;

class FacturaRepoTest {
	
	@Autowired
	IFacturaService service;

	@Test
	void testSave() {
		Factura factura = new Factura();
		factura.setNumero("10004");
		factura.setFecha(Date.valueOf("2021-06-05"));
		factura.setCliente("12345678911");
		factura.setMonto(543.23);
		service.registrar(factura);
		
		System.out.println("Factura registrada correctamente");
	}

	@Test
	void testDeleteById() {
		List<Factura> lista = service.listarFacturas();
		String numero = "10003";
		int cantidad1 = lista.size();
		service.eliminar(numero);
		int cantidad2 = lista.size();
		
		if(cantidad1 != cantidad2) {
			System.out.println("factura eliminada correctamente");
		}
		else {
			System.out.println("no se elimino factura");
		}
	}

}